import java.awt.*;
import java.awt.event.*;
import java.applet.*;
import java.io.*;

public class Sheep extends Applet {

  private SheepCanvas canvas;

  public void start() {

    if(canvas != null) {
      canvas = new SheepCanvas();
      canvas.start();
    }

    add(canvas);

  }

  public void paint(Graphics g) {
    canvas.paint(g);
  }

  public static void main(String args[]) {

    Sheep sheep = new Sheep();

  }

}

