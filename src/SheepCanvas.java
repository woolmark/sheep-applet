import java.awt.*;
import java.awt.event.*;
import java.applet.*;
import java.util.*;
import java.io.*;

/**
 * Sheep Canvas.
 *
 * @author takkie
 */
public class SheepCanvas extends Applet implements Runnable {

  private static int i, j, k;

  private static int l, m, n;

  private int width = 120;

  private int height = 120;

  /**
  * Thread.
  */
  private Thread runner;
  
  /**
  * create random number.
  */
  private static Random rand;

  /**
  * the system font type.
  */
  private Font font;

  /**
  * the application state.
  */
  private int state;

  /**
  * the sheep number.
  */
  private int sheep_number;

  /**
  * the sheep image.
  */
  private Image sheep_image[];

  /**
  * the frame image.
  */
  private Image background;

  /**
  * the sheep position.
  */
  private int sheep_pos[][] = new int[100][3];

  /**
  * key flag.
  */
  public boolean flag;

  /**
  * do nothing.
  */
  public SheepCanvas() {

  }

  /**
  * start application.
  */
  public void start() {

    /* create object */
    state = 1;
    rand = new Random();

    sheep_image = new Image[2];

    try {

      sheep_image[0] = Toolkit.getDefaultToolkit().createImage(getClass().getResource("/sheep00.png"));
      sheep_image[1] = Toolkit.getDefaultToolkit().createImage(getClass().getResource("/sheep00.png"));

      background = Toolkit.getDefaultToolkit().createImage(getClass().getResource("/back.png"));

    } catch (Exception e) {
      System.out.println(e);
    }

    sheep_number = readInt("sheep_number");   

    /* set the sheep position */
    for(i = 0; i < sheep_pos.length; i++) {
        sheep_pos[i][0] = 0;
        sheep_pos[i][1] = -1;
        sheep_pos[i][2] = 0;
    }

    sheep_pos[0][0] = (120 - 120 % 10) + 10;
    sheep_pos[0][1] = (120 - 40) + rand.nextInt() % 30;
    sheep_pos[0][2] = 0;

    state = 5;

    /* start running */
    runner = new Thread(this);
    runner.start();

  }

  /**
  * run method.
  */
  public void run() {

    while(true) {

      calc();

      repaint();

      try {
          Thread.sleep(60);
      } catch(Exception exception) {
      }

    }

  }

  /**
  * calculation method.
  */
  private void calc() {

    if(state == 5) {

      /* run the sheep */
      if(flag) {

        /* add a new sheep */
        for(i = 1; i < sheep_pos.length; i++) {

          if(sheep_pos[i][1] == -1) {

            sheep_pos[i][0] = 120 + 20;
            sheep_pos[i][1] = (120 - 40) + rand.nextInt() % 30;
            sheep_pos[i][2] = 0;

            break;

          }

        }

      }

      /* run the sheep */
      for(i = 0; i < sheep_pos.length; i++) {
        if(sheep_pos[i][1] >= 0) {

          /* remove a frameouted sheep */
          if((sheep_pos[i][0] -= 5) < -20) {
            if(i == 0) {
              sheep_pos[0][0] = 120 + 20;
              sheep_pos[0][1] = (120 - 40) + rand.nextInt() % 30;
              sheep_pos[0][2] = 0;
            } else {
                sheep_pos[i][0] = 0;
                sheep_pos[i][1] = -1;
                sheep_pos[i][2] = 0;
            }
          }

          /* run */
          if(sheep_pos[i][0] > 50 && sheep_pos[i][0] <= 70) {
            sheep_pos[i][1] -= 3;
          }
          
          /* jump */
          else {
            if(sheep_pos[i][0] > 30 && sheep_pos[i][0] <= 50) {
                sheep_pos[i][1] += 3;
            }
            if(sheep_pos[i][0] < 60 && sheep_pos[i][2] == 0) {
                sheep_number++;
                sheep_pos[i][2] = 1;
            }
          }
        }

      }

    }

    else {
      if(state != 6);
    }
  }

  public void paint(Graphics g) {
    update(g);
  }

  public void update(Graphics g) {

    if(state == 5) {

      Image image = createImage(120, 120);
      Graphics bg = image.getGraphics();

      bg.setColor(new Color(100, 255, 100));
      bg.fillRect(0, 0, 120, 120);
      bg.setColor(new Color(150, 150, 255));
      bg.fillRect(0, 0, 120, 120 - 70);

      bg.drawImage(background, 35, 120 - 80, null);

      for(l = 0; l < sheep_pos.length; l++) {
        if(sheep_pos[l][1] >= 0) {
          bg.drawImage(sheep_image[1],
            sheep_pos[l][0], sheep_pos[l][1], null);
        }
      }

      bg.setColor(new Color(0, 0, 0));
      bg.drawString(
        "�r��" + sheep_number + "�C",
        5, 15);

      g.drawImage(image, 0, 0, null);

    }

  }

  private int getKeyNum(int keyin) {

    return 31;

  }

  public void writeInt(String file, int param) {

/*

    byte B[] = new byte[4];

    B[3] = (byte)(param & 0x000000ff);
    B[2] = (byte)((param >> 8) & 0x000000ff);
    B[1] = (byte)((param >> 16) & 0x000000ff);
    B[0] = (byte)((param >> 24) & 0x000000ff);

    try {
      RecordStore rs = RecordStore.openRecordStore(file, true);
      RecordEnumeration re = rs.enumerateRecords(null, null, true);

      if (re.hasNextElement()) {
        rs.setRecord(re.nextRecordId(), B, 0, B.length);
      }

      else {
        rs.addRecord(B, 0, B.length);
      }

      rs.closeRecordStore();

    }catch(Exception e) {}

*/

  }

  public int readInt(String file) {

    int param = 0;
    byte[] b = new byte[4];

/*
    try {
      RecordStore rs = RecordStore.openRecordStore(file, true);
      RecordEnumeration re = rs.enumerateRecords(null, null, true);

      if (re.hasNextElement()) {
        b = rs.getRecord(re.nextRecordId());

        param |= (int)((b[0] << 24) & 0xff000000);
        param |= (int)((b[1] << 16) & 0x00ff0000);
        param |= (int)((b[2] << 8) & 0x0000ff00);
        param |= (int)(b[3] & 0x000000ff);
      }

      rs.closeRecordStore();

    }catch(Exception e) {}

*/

    return param;

  }

}
